package de.onyxbits.textfiction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

import android.os.Environment;

import static android.os.Environment.DIRECTORY_DOCUMENTS;

public class FileUtil implements Comparator<File> {

	/**
	 * Datadir on the external storage
	 */
	public static final String HOMEDIR = "TextFiction";

	/**
	 * Where the games are stored (relative to the HOMEDIR or app data dir).
	 */
	public static final String GAMEDIR = "games";

	/**
	 * Where the save game files are stored (relative to the HOMEDIR or app data
	 * dir).
	 */
	public static final String SAVEDIR = "savegames";

	/**
	 * Where games store misc data
	 */
	public static final String DATADIR = "gamedata";

	private static final File library;
	private static final File saves;
	private static final File data;

	static {
		File root = Environment.getExternalStoragePublicDirectory(DIRECTORY_DOCUMENTS);
		library = new File(new File(root, HOMEDIR), GAMEDIR);
		saves = new File(new File(root, HOMEDIR), SAVEDIR);
		data = new File(new File(root, HOMEDIR), DATADIR);
	}
	
	private static void ensureDirs(){
		boolean wasSuccessfulLibrary = library.mkdirs();
		if (!wasSuccessfulLibrary) { System.out.println("was not successful."); }
		boolean wasSuccessfulSaves = saves.mkdirs();
		if (!wasSuccessfulSaves) { System.out.println("was not successful."); }
		boolean wasSuccessfulData = data.mkdirs();
		if (!wasSuccessfulData) { System.out.println("was not successful."); }
	}

	public static File[] listGames() {
		ensureDirs();
		File[] ret = library.listFiles();
		if (ret == null) {
			return new File[0];
		}
		Arrays.sort(ret);

		return ret;
	}

	/**
	 * List all the save files for a game
	 * 
	 * @param game
	 *          the game in question
	 * @return list of files in the savegamedir
	 */
	public static File[] listSaveGames(File game) {
		ensureDirs();
		File f = getSaveGameDir(game);
		File[] ret = f.listFiles();
		if (ret == null) {
			return new File[0];
		}
		Arrays.sort(ret, new FileUtil());
		return ret;
	}

	/**
	 * List all the saves for a game
	 * 
	 * @param game
	 *          the game in question
	 * @return the filenames of the save games.
	 */
	public static String[] listSaveName(File game) {
		ensureDirs();
		File[] f = listSaveGames(game);
		String[] ret = new String[f.length];
		for (int i = 0; i < ret.length; i++) {
			ret[i] = f[i].getName();
		}
		return ret;
	}

	/**
	 * 
	 */
	public FileUtil() {
	}

	/**
	 * Copies files to the library
	 * 
	 * @param src
	 *          the file to copy
	 * @throws IOException
	 *           if something goes wrong
	 */
	public static void importGame(File src) throws IOException {
		ensureDirs();
		File dst = new File(library, src.getName());
		byte[] buf = new byte[1024];
		FileInputStream fin = new FileInputStream(src);
		FileOutputStream fout = new FileOutputStream(dst);
		int len;
		while ((len = fin.read(buf)) > 0) {
			fout.write(buf, 0, len);
		}
		boolean wasSuccessful = getSaveGameDir(dst).mkdirs();
		if (!wasSuccessful) { System.out.println("was not successful."); }
		fin.close();
		fout.close();
	}

	/**
	 * Delete a game and all other files belonging to it.
	 * 
	 * @param game
	 *          the game file
	 */
	public static void deleteGame(File game) {
		ensureDirs();
		File[] lst = getSaveGameDir(game).listFiles();
		assert lst != null;
		for (File f : lst) {
			boolean wasSuccessful = f.delete();
			if (!wasSuccessful) { System.out.println("was not successful."); }
		}

		lst = getDataDir(game).listFiles();
		assert lst != null;
		for (File f : lst) {
			boolean wasSuccessful = f.delete();
			if (!wasSuccessful) { System.out.println("was not successful."); }
		}
		boolean wasSuccessfulSave = getSaveGameDir(game).delete();
		if (!wasSuccessfulSave) { System.out.println("was not successful."); }
		boolean wasSuccessfulData = getDataDir(game).delete();
		if (!wasSuccessfulData) { System.out.println("was not successful."); }
		boolean wasSuccessfulGame = game.delete();
		if (!wasSuccessfulGame) { System.out.println("was not successful."); }
	}

	/**
	 * Get the directory where a game keeps its savegames
	 * 
	 * @param game
	 *          the game in question
	 * @return a directory for saving games.
	 */
	public static File getSaveGameDir(File game) {
		ensureDirs();
		File ret = new File(saves, game.getName());
		boolean wasSuccessful = ret.mkdirs();
		if (!wasSuccessful) { System.out.println("was not successful."); }
		return ret;
	}

	/**
	 * Get the directory where a game may keep various (config) data.
	 * 
	 * @param game
	 *          the game in question.
	 * @return a directory for keeping misc data.
	 */
	public static File getDataDir(File game) {
		ensureDirs();
		File ret = new File(data, game.getName());
		boolean wasSuccessful = ret.mkdirs();
		if (!wasSuccessful) { System.out.println("was not successful."); }
		return ret;
	}

	/**
	 * Strip filename extension from file name
	 * 
	 * @param file
	 *          the file in question
	 * @return basename
	 */
	public static String basename(File file) {
		String tmp = file.getName();
		int idx = tmp.lastIndexOf('.');
		if (idx > 0) {
			return tmp.substring(0, idx);
		}
		else {
			return tmp;
		}
	}

	/**
	 * Read a text file
	 * @param file the file to read
	 * @return its contents as a String
	 * @throws IOException if stuff goes wrong.
	 */
	public static String getContents(File file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line;
		StringBuilder stringBuilder = new StringBuilder();
		String ls = System.getProperty("line.separator");

		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line);
			stringBuilder.append(ls);
		}
		reader.close();
		return stringBuilder.toString();
	}

	@Override
	public int compare(File lhs, File rhs) {
		return Long.compare(rhs.lastModified(), lhs.lastModified());
	}
}
