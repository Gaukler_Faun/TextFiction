package de.onyxbits.textfiction;

import java.io.File;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Objects;

import org.json.JSONArray;

import de.onyxbits.textfiction.input.CmdIcon;
import de.onyxbits.textfiction.input.CommandChanger;
import de.onyxbits.textfiction.input.InputProcessor;
import de.onyxbits.textfiction.input.WordExtractor;
import de.onyxbits.textfiction.zengine.GrueException;
import de.onyxbits.textfiction.zengine.StyleRegion;
import de.onyxbits.textfiction.zengine.ZMachine;
import de.onyxbits.textfiction.zengine.ZState;
import de.onyxbits.textfiction.zengine.ZStatus;
import de.onyxbits.textfiction.zengine.ZWindow;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;

import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.color.DynamicColors;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

/**
 * The activity where actual gameplay takes place.
 * 
 * @author patrick
 * 
 */
public class GameActivity extends FragmentActivity implements DialogInterface.OnClickListener, InputProcessor, PopupMenu.OnMenuItemClickListener {

	/**
	 * Name of the file we keep our highlights in
	 */
	public static final String HIGHLIGHTFILE = "highlights.json";

	/**
	 * This activity must be started through an intent and be passed the filename
	 * of the game via this extra.
	 */
	public static final String LOADFILE = "loadfile";

	/**
	 * How many items to keep in the messagebuffer at most. Note: this should be
	 * an odd number so the log starts with a narrator entry.
	 */
	public static final int MAXMESSAGES = 81;
	private static final int PENDING_NONE = 0;
	private static final int PENDING_RESTORE = 2;

	/**
	 * Displays the message log
	 */
	private ListView storyBoard;

	/**
	 * Adapter for the story list
	 */
	private StoryAdapter messages;

	/**
	 * The "upper window" of the z-machine containing the status part
	 */
	private TextView statusWindow;

	/**
	 * Holds stuff that needs to survive config changes (e.g. screen rotation).
	 */
	private RetainerFragment retainerFragment;

	/**
	 * Contains story- and status screen.
	 */
	private ViewFlipper windowFlipper;

	/**
	 * The game playing in this activity
	 */
	private File storyFile;

	/**
	 * State variable for when we are showing a "confirm" dialog.
	 */
	private int pendingAction = PENDING_NONE;

	/**
	 * Words we are highligting in the story
	 */
	private String[] highlighted;
	private SharedPreferences prefs;

	/**
	 * Filename in the data dir where we keep user defined button values.
	 */
	public static final String COMPASSFILE = "compass.json";
	private EditText cmdLine;

	/**
	 * One button per cardinal direction. The command of the button is bound to it
	 * through setTag().
	 */

	private InputProcessor inputProcessor;


	/**
	 * Name of the file (relative to the gamedata dir where to quick commmands
	 * settings are kept.
	 */
	public static final String CMDFILE = "quickcommands.json";

	@SuppressLint("NonConstantResourceId")
	@Override
	public boolean onMenuItemClick(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.flip:
				flipView(windowFlipper.getCurrentView() != storyBoard);
				return true;
			case R.id.save:
				MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(GameActivity.this);
				View dialogViewSub = View.inflate(GameActivity.this, R.layout.dialog_save, null);
				EditText editSave = dialogViewSub.findViewById(R.id.editSave);
				builder.setView(dialogViewSub);
				builder.setTitle(R.string.title_save_game);
				builder.setPositiveButton(R.string.app_ok, (dialog2, whichButton) -> {
					String name = editSave.getEditableText().toString();
					name = name.replace('/', '_');
					if (name.length() > 0) {
						ZState state = new ZState(retainerFragment.engine);
						File f = new File(FileUtil.getSaveGameDir(storyFile), name);
						state.disk_save(f.getPath(), retainerFragment.engine.pc);
						Toast.makeText(this, R.string.msg_game_saved, Toast.LENGTH_SHORT).show();
					}
				});
				builder.setNegativeButton(R.string.app_cancel, (dialog, whichButton) -> dialog.cancel());
				Dialog dialog = builder.create();
				dialog.show();
				Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.BOTTOM);
				return true;
			case R.id.restore:
				String[] sg = FileUtil.listSaveName(storyFile);
				if (sg.length > 0) {
					pendingAction = PENDING_RESTORE;
					MaterialAlertDialogBuilder builderSub = new MaterialAlertDialogBuilder(GameActivity.this);
					builderSub.setTitle(R.string.title_restore_game);
					builderSub.setItems(sg, this);
					builderSub.setNegativeButton(R.string.app_cancel, (dialog2, whichButton) -> dialog2.cancel());
					Dialog dialogSub = builderSub.create();
					dialogSub.show();
					Objects.requireNonNull(dialogSub.getWindow()).setGravity(Gravity.BOTTOM);
				} else {
					Toast.makeText(this, R.string.msg_no_savegames, Toast.LENGTH_SHORT).show();
				}
				return true;
			case R.id.clear:
				retainerFragment.messageBuffer.clear();
				messages.notifyDataSetChanged();
				return true;
			case R.id.restart:
				try {
					retainerFragment.engine.restart();
					retainerFragment.engine.run();
				}
				catch (GrueException e) {
					// This should never happen
					retainerFragment.postMortem = e;
					finish();
				}
				publishResult();
				return true;
		}
		return  false;
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		DynamicColors.applyToActivitiesIfAvailable(this.getApplication());
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		prefs = androidx.preference.PreferenceManager.getDefaultSharedPreferences(GameActivity.this);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);

		storyFile = new File(getIntent().getStringExtra(LOADFILE));
		View content = View.inflate(this, R.layout.activity_game, null);
		setContentView(content);

		try {
			inputProcessor = this;
		} catch (ClassCastException e) {
			throw new ClassCastException(this.toString() + " must implement OnArticleSelectedListener");
		}

		ImageButton expand = findViewById(R.id.expand);
		//expand.setOnClickListener(v -> showGameMenu());

		expand.setOnClickListener(view -> {
			PopupMenu popup = new PopupMenu(GameActivity.this, view);
			popup.setOnMenuItemClickListener(GameActivity.this);
			popup.inflate(R.menu.menu);
			popup.show();
		});

		// Compass stuff

		String[] cmds = new String[4];

		// Load user defined directions
		try {
			File file = new File(FileUtil.getDataDir(inputProcessor.getStory()), COMPASSFILE);
			JSONArray js = new JSONArray(FileUtil.getContents(file));
			for (int i = 0; i < 4; i++) {
				cmds[i] = js.getString(i);
			}
		} catch (Exception e) {
			// No big deal. Probably the first time this game runs -> use defaults
			cmds = getResources().getStringArray(R.array.initial_compass);
		}

		ImageButton compass = findViewById(R.id.compass);
		String[] finalCmds = cmds;
		compass.setOnTouchListener(new SwipeTouchListener(this) {
			public void onSwipeTop() {
				hideKeyBoard();
				executeCommand((finalCmds[0] + "\n").toCharArray());
			}
			public void onSwipeBottom() {
				hideKeyBoard();
				inputProcessor.executeCommand((finalCmds[2] + "\n").toCharArray());
			}
			public void onSwipeRight() {
				hideKeyBoard();
				inputProcessor.executeCommand((finalCmds[1] + "\n").toCharArray());
			}
			public void onSwipeLeft() {
				hideKeyBoard();
				inputProcessor.executeCommand((finalCmds[3] + "\n").toCharArray());
			}
		});

		compass.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {


				MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(GameActivity.this);
				View dialogView = View.inflate(GameActivity.this, R.layout.dialog_edit_compass, null);
				builder.setView(dialogView);

				EditText editCommand0 = dialogView.findViewById(R.id.editCommand0);
				EditText editCommand1 = dialogView.findViewById(R.id.editCommand1);
				EditText editCommand2 = dialogView.findViewById(R.id.editCommand2);
				EditText editCommand3 = dialogView.findViewById(R.id.editCommand3);

				try {
					editCommand0.setText(finalCmds[0]);
					editCommand1.setText(finalCmds[1]);
					editCommand2.setText(finalCmds[2]);
					editCommand3.setText(finalCmds[3]);
				} catch (Exception e) {
					Log.w(getClass().getName(), e); // ?!
				}

				builder.setView(dialogView);
				builder.setTitle(R.string.title_edit_direction);
				builder.setPositiveButton(R.string.app_ok, (dialog, whichButton) -> {
					String tmp0 = editCommand0.getText().toString();
					String tmp1 = editCommand1.getText().toString();
					String tmp2 = editCommand2.getText().toString();
					String tmp3 = editCommand3.getText().toString();
					try {
						File file = new File(FileUtil.getDataDir(inputProcessor.getStory()), COMPASSFILE);
						JSONArray array = new JSONArray();
						array.put(tmp0);
						array.put(tmp1);
						array.put(tmp2);
						array.put(tmp3);
						PrintWriter pw = new PrintWriter(file);
						pw.write(array.toString(2));
						pw.close();
						Toast.makeText(GameActivity.this, R.string.app_reload, Toast.LENGTH_SHORT).show();
					} catch (Exception e) {
						Log.w(getClass().getName(), e);
					}
				});
				builder.setNegativeButton(R.string.app_cancel, (dialog, whichButton) -> dialog.cancel());
				androidx.appcompat.app.AlertDialog dialog = builder.create();
				dialog.show();
				Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.BOTTOM);
				return false;
			}
		});

		// input stuff

		LinearLayout buttonBar = findViewById(R.id.quickcmdcontainer);
		File commands = new File(FileUtil.getDataDir(inputProcessor.getStory()), CMDFILE);
		cmdLine = findViewById(R.id.userinput);
		cmdLine.setOnEditorActionListener((v, actionId, event) -> {
			executeCommand();
			return true;
		});

		ImageButton submit = findViewById(R.id.submit);
		submit.setOnClickListener(v -> executeCommand());

		CommandChanger changer = new CommandChanger(cmdLine, buttonBar, commands);
		try {
			String buttonDef = getString(R.string.defaultcommands);
			JSONArray buttons = new JSONArray(buttonDef);
			if (commands.exists()) {
				buttonDef = FileUtil.getContents(commands);
				buttons = new JSONArray(buttonDef);
			}
			for (int i = 0; i < buttons.length(); i++) {
				ImageButton b = View.inflate(this, R.layout.style_cmdbutton, null).findViewById(R.id.protocmdbutton);
				CmdIcon ico = CmdIcon.fromJSON(buttons.getJSONObject(i));
				b.setTag(ico);
				b.setImageResource(CmdIcon.ICONS[ico.imgid]);
				b.setOnClickListener(v -> {
					CmdIcon ci = (CmdIcon) v.getTag();
					if (ci.atOnce) {
						hideKeyBoard();
						inputProcessor.executeCommand((ci.cmd + "\n").toCharArray());
					} else {
						// Allow the player to select either the verb or an object first. If
						// an object is selected first and a verb second, assume the input to
						// be finished and execute it. The other way around: allow more objects
						// to be added.
						String tmp = cmdLine.getEditableText().toString().trim();
						String text = ci.cmd.trim() + " " + tmp;
						cmdLine.setText(text);
						cmdLine.setSelection(cmdLine.getEditableText().toString().length());
						if (tmp.length() > 0) {
							executeCommand();
						}
					}
				});
				b.setOnLongClickListener(changer);
				b.setContentDescription(ico.cmd);
				buttonBar.addView(b);
			}
		} catch (Exception e) {
			Log.w(getClass().getName(), e);
		}

		// Check if this is a genuine start or if we are restarting because the
		// device got rotated.
		FragmentManager fm = getSupportFragmentManager();
		retainerFragment = (RetainerFragment) fm.findFragmentByTag("retainer");
		if (retainerFragment == null) {
			// First start
			retainerFragment = new RetainerFragment();
			fm.beginTransaction().add(retainerFragment, "retainer").commit();
		}

		// Load the highlight file
		try {
			File file = new File(FileUtil.getDataDir(storyFile), HIGHLIGHTFILE);
			JSONArray js = new JSONArray(FileUtil.getContents(file));
			for (int i = 0; i < js.length(); i++) {
				retainerFragment.highlighted.add(js.getString(i));
			}
		} catch (Exception e) {
			// No big deal. Probably the first time this game runs -> use defaults
			String[] ini = getResources().getStringArray(R.array.initial_highlights);
			retainerFragment.highlighted.addAll(Arrays.asList(ini));
		}
		highlighted = retainerFragment.highlighted.toArray(new String[0]);
		WordExtractor wordExtractor = new WordExtractor(this);
		wordExtractor.setInputActivity(this);
		wordExtractor.setInputProcessor(this);
		wordExtractor.setKeyclick(prefs.getBoolean("keyclick", false));
		messages = new StoryAdapter(this, 0, retainerFragment.messageBuffer, wordExtractor);

		storyBoard = content.findViewById(R.id.storyboard);
		storyBoard.setAdapter(messages);

		windowFlipper = content.findViewById(R.id.window_flipper);
		statusWindow = content.findViewById(R.id.status);
		statusWindow.setText(retainerFragment.upperWindow);

		String fontSize = prefs.getString("fontsize", "");
		TextView tmp = new TextView(this);
		if (fontSize.equals("small")) {
			tmp.setTextAppearance( android.R.style.TextAppearance_Small);
			messages.setTextSize(tmp.getTextSize());
		}
		if (fontSize.equals("medium")) {
			tmp.setTextAppearance(android.R.style.TextAppearance_Medium);
			messages.setTextSize(tmp.getTextSize());
		}
		if (fontSize.equals("large")) {
			tmp.setTextAppearance(android.R.style.TextAppearance_Large);
			messages.setTextSize(tmp.getTextSize());
		}
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {

		if (retainerFragment == null || retainerFragment.engine == null) {
			assert retainerFragment != null;
			if (retainerFragment.postMortem != null) {
				// Let's not go into details here. The user won't understand them
				// anyways.
				Toast.makeText(this, R.string.msg_corrupt_game_file, Toast.LENGTH_SHORT).show();
			}
			super.onDestroy();
			return;
		}

		if (retainerFragment.postMortem != null) {
			// Let's not go into details here. The user won't understand them anyways.
			Toast.makeText(this, R.string.msg_corrupt_game_file, Toast.LENGTH_SHORT).show();
			super.onDestroy();
			return;
		}

		if (retainerFragment.engine.getRunState() == ZMachine.STATE_WAIT_CMD) {
			ZState state = new ZState(retainerFragment.engine);
			File f = new File(FileUtil.getSaveGameDir(storyFile), getString(R.string.autosavename));
			state.disk_save(f.getPath(), retainerFragment.engine.pc);
		} else {
			Toast.makeText(this, R.string.mg_not_at_a_commandprompt, Toast.LENGTH_LONG).show();
		}
		super.onDestroy();
	}

	@Override
	public void executeCommand(char[] inputBuffer) {
		ZMachine engine = retainerFragment.engine;
		if (engine != null && engine.getRunState() != ZMachine.STATE_RUNNING) {
			retainerFragment.engine.fillInputBuffer(inputBuffer);
			if (retainerFragment.engine.getRunState() != ZMachine.STATE_WAIT_CHAR) {
				String tmp = new String(inputBuffer).replaceAll("\n", "").trim();
				SpannableString ss = new SpannableString(tmp);
				retainerFragment.messageBuffer.add(new StoryItem(ss, StoryItem.MYSELF));
			}
			try {
				retainerFragment.engine.run();
				publishResult();
				if (retainerFragment.engine.saveCalled || retainerFragment.engine.restoreCalled) {
					// This is a really ugly hack to let the user know that the
					// save/restore commands
					// don't work
					Toast.makeText(this, R.string.err_sr_deprecated, Toast.LENGTH_LONG).show();
					retainerFragment.engine.saveCalled = false;
					retainerFragment.engine.restoreCalled = false;
				}
			} catch (GrueException e) {
				retainerFragment.postMortem = e;
				Log.w(getClass().getName(), e);
				finish();
			}
		}
	}

	/**
	 * Callback: publish results after the engine has run
	 */
	public void publishResult() {

		ZWindow upper = retainerFragment.engine.window[1];
		ZWindow lower = retainerFragment.engine.window[0];
		ZStatus status = retainerFragment.engine.status_line;
		String tmp;
		boolean showLower = false;

		// Evaluate game status
		if (status != null) {
			// Z3 game -> copy the status bar object into the upper window.
			retainerFragment.engine.update_status_line();
			retainerFragment.upperWindow = status.toString();
			statusWindow.setText(retainerFragment.upperWindow);
		}
		else {
			if (upper.maxCursor > 0) {
				// The normal, "status bar" upper window.
				tmp = upper.stringyfy(upper.startWindow, upper.maxCursor);
			}
			else {
				tmp = "";
			}
			statusWindow.setText(tmp);
			retainerFragment.upperWindow = tmp;
		}
		upper.retrieved();

		// Evaluate story progress
		if (lower.cursor > 0) {
			showLower = true;
			tmp = new String(lower.frameBuffer, 0, lower.noPrompt());
			SpannableString stmp = new SpannableString(tmp);
			StyleRegion reg = lower.regions;
			if (reg != null) {
				while (reg != null) {
					if (reg.next == null) {
						// The printer does not "close" the last style since it doesn't know
						// when the last character is printed.
						reg.end = tmp.length() - 1;
					}
					// Did the game style the prompt (which we cut away)?
					reg.end = Math.min(reg.end, tmp.length() - 1);
					switch (reg.style) {
						case ZWindow.BOLD: {
							stmp.setSpan(new StyleSpan(Typeface.BOLD), reg.start, reg.end, 0);
							break;
						}
						case ZWindow.ITALIC: {
							stmp.setSpan(new StyleSpan(Typeface.ITALIC), reg.start, reg.end, 0);
							break;
						}
						case ZWindow.FIXED: {
							stmp.setSpan(new TypefaceSpan("monospace"), reg.start, reg.end, 0);
							break;
						}
					}
					reg = reg.next;
				}
			}
			highlight(stmp, highlighted);
			try {
				retainerFragment.messageBuffer.add(new StoryItem(stmp, StoryItem.NARRATOR));
			}
			catch (IndexOutOfBoundsException e) {
				// This is a workaround! Some games manage to mess up the spans. It is
				// better to loose the styles than to crash the game.
				retainerFragment.messageBuffer.add(new StoryItem(new SpannableString(tmp),
						StoryItem.NARRATOR));
			}
		}
		lower.retrieved();

		// Throw out old story items.
		while (retainerFragment.messageBuffer.size() > MAXMESSAGES) {
			retainerFragment.messageBuffer.remove(0);
		}
		messages.notifyDataSetChanged();

		// Scroll the storyboard to the latest item.
		if (prefs.getBoolean("smoothscrolling", true)) {
			// NOTE:smoothScroll() does not work properly if the theme defines
			// dividerheight > 0!
			storyBoard.smoothScrollToPosition(retainerFragment.messageBuffer.size() - 1);
		} else {
			storyBoard.setSelection(retainerFragment.messageBuffer.size() - 1);
		}
		// Kinda dirty: assume that the lower window is the important one. If
		// anything got added to it, ensure that it is visible. Otherwise assume
		// that we are dealing with something like a menu and switch the display to
		// display the upperwindow
		flipView(showLower);
	}

	/**
	 * Make either the storyboard or the statusscreen visible
	 * 
	 * @param showstory
	 *          true to swtich to the story view, false to swtich to the status
	 *          screen. nothing happens if the desired view is already showing.
	 */
	private void flipView(boolean showstory) {
		View now = windowFlipper.getCurrentView();
		if (showstory) {
			if (now != storyBoard) {
				windowFlipper.showPrevious();
			}
		} else {
			if (now == storyBoard) {
				windowFlipper.setInAnimation(AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left));
				windowFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right));
				windowFlipper.showPrevious();
			}
		}
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		if (pendingAction == PENDING_RESTORE) {
			if (which > -1) {
				File file = FileUtil.listSaveGames(storyFile)[which];
				ZState state = new ZState(retainerFragment.engine);
				if (state.restore_from_disk(file.getPath())) {
					statusWindow.setText(""); // Wrong, but the best we can do.
					retainerFragment.messageBuffer.clear();
					messages.notifyDataSetChanged();
					retainerFragment.engine.restore(state);
					Toast.makeText(this, R.string.msg_game_restored, Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(this, R.string.msg_restore_failed, Toast.LENGTH_SHORT).show();
				}
			}
		}
		pendingAction = PENDING_NONE;
	}

	@Override
	public void toggleTextHighlight(String str) {
		int tmp;
		String txt = str.toLowerCase();
		if (retainerFragment.highlighted.contains(txt)) {
			retainerFragment.highlighted.remove(txt);
			tmp = R.string.msg_unmarked;
		}
		else {
			retainerFragment.highlighted.add(txt);
			tmp = R.string.msg_marked;
		}
		Toast.makeText(this, getResources().getString(tmp, txt), Toast.LENGTH_SHORT).show();
		highlighted = retainerFragment.highlighted.toArray(new String[0]);
		for (StoryItem storyItem : retainerFragment.messageBuffer) {
			highlight(storyItem.message, highlighted);
		}
		messages.notifyDataSetChanged();
		try {
			JSONArray array = new JSONArray(retainerFragment.highlighted);
			File f = new File(FileUtil.getDataDir(storyFile), HIGHLIGHTFILE);
			PrintStream ps = new PrintStream(f);
			ps.write(array.toString(2).getBytes());
			ps.close();
		}
		catch (Exception e) {
			Log.w(getClass().getName(), e);
		}
	}

	/**
	 * Add underlines to a text blob. Any existing underlines are removed. before
	 * new ones are added.
	 * 
	 * @param span
	 *          the blob to modify
	 * @param words
	 *          the words to underline (all lowercase!)
	 */
	private static void highlight(SpannableString span, String... words) {
		UnderlineSpan[] old = span.getSpans(0, span.length(), UnderlineSpan.class);
		for (UnderlineSpan del : old) {
			span.removeSpan(del);
		}
		char[] spanChars = span.toString().toLowerCase().toCharArray();
		for (String word : words) {
			char[] wc = word.toCharArray();
			int last = spanChars.length - wc.length + 1;
			for (int i = 0; i < last; i++) {
				// First check if there is a word-sized gap at spanchars[i] as we don't
				// want to highlight words that are actually just substrings (e.g.
				// "east" in "lEASTwise").
				if ((i > 0 && Character.isLetterOrDigit(spanChars[i - 1]))
						|| (i + wc.length != spanChars.length && Character.isLetterOrDigit(spanChars[i
								+ wc.length]))) {
					continue;
				}
				int a = i;
				int b = 0;
				while (b < wc.length) {
					if (spanChars[a] != wc[b]) {
						b = 0;
						break;
					}
					a++;
					b++;
				}
				if (b == wc.length) {
					span.setSpan(new UnderlineSpan(), i, a, 0);
					i = a;
				}
			}
		}
	}

	@Override
	public File getStory() {
		return storyFile;
	}

	private void executeCommand() {
		inputProcessor.executeCommand((cmdLine.getText().toString() + "\n").toCharArray());
		hideKeyBoard();
	}

	private void hideKeyBoard () {
		cmdLine.setText("");
		cmdLine.clearFocus();
		if (cmdLine != null && prefs.getBoolean("autocollapse", false)) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(cmdLine.getWindowToken(), 0);
		}
	}

	/**
	 * Call after running a command to clear the commandline
	 */
	public void reset() {
		cmdLine.setText("");
	}

	/**
	 * Append a "word" to the prompt.
	 *
	 * @param str
	 *          the string to add (does not require whitespaces at the start or
	 *          end).
	 */
	public void appendWord(String str) {
		if (str.length() > 0) {
			String tmp = cmdLine.getText().toString().trim();
			tmp = tmp.trim() + " " + str.trim();
			cmdLine.setText(tmp);
			cmdLine.setSelection(tmp.length());
		}
	}

	/**
	 * Delete the rightmost word on the prompt.
	 */
	public void removeWord() {
		String tmp = cmdLine.getText().toString().trim();
		int idx = tmp.lastIndexOf(' ');
		if (idx > 0) {
			tmp = tmp.substring(0, idx);
			cmdLine.setText(tmp);
			cmdLine.setSelection(tmp.length());
		}
		else {
			reset();
		}
	}
}