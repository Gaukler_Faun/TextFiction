package de.onyxbits.textfiction.input;

import java.io.File;

/**
 * Master callback interface of the package.
 * 
 * @author patrick
 * 
 */
public interface InputProcessor {

	/**
	 * Make the game engine process user input. Calling this method while the
	 * engine is not idle results in a no-op.
	 * 
	 * @param inputBuffer
	 *          user input to hand over to the engine
	 */
	void executeCommand(char[] inputBuffer);
	
	/**
	 * Turn highlighting of text in the story on/off.
	 * @param txt the "word" to toggle highlighting for.
	 */
	void toggleTextHighlight(String txt);

	/**
	 * Query the game that is being played.
	 * @return path to the currently playing game
	 */
	File getStory();
}
