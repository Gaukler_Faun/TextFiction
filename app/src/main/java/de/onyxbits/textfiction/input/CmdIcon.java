package de.onyxbits.textfiction.input;

import org.json.JSONException;
import org.json.JSONObject;

import de.onyxbits.textfiction.R;

/**
 * Container class that stores the info for quick access command buttons.
 * 
 * @author patrick
 * 
 */
public class CmdIcon {


	/**
	 * List of all icon resource ids, the user is allowed to assign to a button
	 */
	public static final int[] ICONS = {
			R.drawable.ic_0,
			R.drawable.ic_1,
			R.drawable.ic_2,
			R.drawable.ic_3,
			R.drawable.ic_4,
			R.drawable.ic_5,
			R.drawable.ic_6,
			R.drawable.ic_7,
			R.drawable.ic_8,
			R.drawable.ic_9,
			R.drawable.ic_10,
			R.drawable.ic_11,
			R.drawable.ic_12,
			R.drawable.ic_13,
			R.drawable.ic_14,
			R.drawable.ic_15,
			R.drawable.ic_16,
			R.drawable.ic_17,
			R.drawable.ic_18,
			R.drawable.ic_19,
			R.drawable.ic_20,
			R.drawable.ic_21,
			R.drawable.ic_22,
			R.drawable.ic_23,
			R.drawable.ic_24,
			R.drawable.ic_25,
			R.drawable.ic_26,
			R.drawable.ic_27,
			R.drawable.ic_28,
			R.drawable.ic_29,
			R.drawable.ic_30,
			R.drawable.ic_31,
	};

	/**
	 * the (user) command (string) associated with the button.
	 */
	public String cmd;

	/**
	 * Whether or not the command should be executed immediately on the button
	 * press.
	 */
	public boolean atOnce;

	/**
	 * ID number of the drawable (for saving in the preferences storage)
	 */
	public int imgid;

	public CmdIcon(int imgid, String cmd, boolean atOnce) {
		this.imgid = imgid;
		this.cmd = cmd;
		this.atOnce = atOnce;
	}

	public static JSONObject toJSON(CmdIcon ico) {
		JSONObject ret = new JSONObject();
		try {
			ret.put("imgid", ico.imgid);
			ret.put("cmd", ico.cmd);
			ret.put("atonce", ico.atOnce);
		}
		catch (JSONException e) {
			// Can't really see this happening
			throw new RuntimeException(e);
		}
		return ret;
	}

	public static CmdIcon fromJSON(JSONObject in) {
		int imgid = in.optInt("imgid", 0);
		if (imgid > ICONS.length - 1) {
			imgid = 0;
		}
		String cmd = in.optString("cmd", "???");
		boolean atOnce = in.optBoolean("atonce", false);
		return new CmdIcon(imgid, cmd, atOnce);
	}
}
