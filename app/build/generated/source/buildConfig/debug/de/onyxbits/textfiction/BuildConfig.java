/**
 * Automatically generated file. DO NOT MODIFY
 */
package de.onyxbits.textfiction;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "de.onyxbits.textfiction";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 12;
  public static final String VERSION_NAME = "3.0";
}
