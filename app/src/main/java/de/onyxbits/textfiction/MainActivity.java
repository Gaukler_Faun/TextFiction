package de.onyxbits.textfiction;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.Html;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.WindowInsetsControllerCompat;

import com.google.android.material.color.DynamicColors;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Build.VERSION.SDK_INT;

/**
 * From this activity, the player can manage his/her library and start games.
 * 
 * @author patrick
 * 
 */
public class MainActivity extends Activity {

	public static final int PERMISSION_REQUEST_CODE = 123;

	/**
	 * The Adapter which will be used to populate the ListView/GridView with
	 * Views.
	 */
	private LibraryAdapter mAdapter;
	private ArrayList<File> games;
	private AbsListView mListView;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */

	public boolean checkPermissionStorage() {
		if (SDK_INT >= Build.VERSION_CODES.R) {
			return Environment.isExternalStorageManager();
		} else {
			int result = ContextCompat.checkSelfPermission(MainActivity.this, READ_EXTERNAL_STORAGE);
			int result1 = ContextCompat.checkSelfPermission(MainActivity.this, WRITE_EXTERNAL_STORAGE);
			return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		DynamicColors.applyToActivitiesIfAvailable(this.getApplication());
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);
		initApp();

		ImageButton fab = findViewById(R.id.fab);
		fab.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, SettingsActivity.class)));

		ImageButton fabInfo = findViewById(R.id.fabInfo);
		fabInfo.setOnClickListener(v -> {
			String url = "https://github.com/scoute-dich/TextFiction";
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(browserIntent);
		});

		games = new ArrayList<>();
		mAdapter = new LibraryAdapter(this, 0, games);
		mAdapter.setStripSuffix(true);

		SpannableString s;
		s = new SpannableString(Html.fromHtml(getString(R.string.app_empty_library),Html.FROM_HTML_MODE_LEGACY));
		Linkify.addLinks(s, Linkify.WEB_URLS);

		TextView empty = findViewById(R.id.empty);
		empty.setText(s);
		empty.setMovementMethod(LinkMovementMethod.getInstance());

		// Set the adapt
		mListView = findViewById(android.R.id.list);
		//mListView.setEmptyView(findViewById(android.R.id.empty));
		((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

		// Set OnItemClickListener so we can be notified on item clicks
		mListView.setOnItemClickListener((parent, view, position, id) -> {
			File game = mAdapter.getItem(position);
			Intent intent = new Intent(MainActivity.this, GameActivity.class);
			intent.putExtra(GameActivity.LOADFILE, game.getAbsolutePath());
			startActivity(intent);
		});
		reScan();
	}

	/**
	 * Drop the current list of games and rebuild it from scratch.
	 */
	public void reScan() {
		File[] stories = FileUtil.listGames();
		games.clear();
		games.addAll(Arrays.asList(stories));
		mAdapter.notifyDataSetChanged();

		if (mListView.getAdapter().getCount() > 0) {
			mListView.setVisibility(View.VISIBLE);
		} else {
			mListView.setVisibility(View.GONE);
		}

	}

	private void initApp () {
		if (checkPermissionStorage()) {
			Uri game = getIntent().getData();
			if (game != null && game.getScheme().equals(ContentResolver.SCHEME_FILE)) {
				File[] f = { new File(game.getPath()) };
				ExecutorService executor = Executors.newSingleThreadExecutor();
				Handler handler = new Handler(Looper.getMainLooper());
				executor.execute(() -> {
					//Background work here
					for (File file : f) {
						try {
							FileUtil.importGame(file);
						} catch (Exception e) {
							Toast.makeText(this, R.string.msg_failure_import, Toast.LENGTH_SHORT).show();
							Log.w(getClass().getName(), e);
							return;
						}
					}

					handler.post(() -> {
						//UI Thread work here
					});
				});
			}
		} else {
			MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
			builder.setMessage(R.string.app_permission_sdCard);
			builder.setPositiveButton(R.string.app_ok, (dialog, id) -> {
				dialog.cancel();
				if (SDK_INT >= Build.VERSION_CODES.R) {
					try {
						Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
						intent.addCategory("android.intent.category.DEFAULT");
						intent.setData(Uri.parse(String.format("package:%s",getPackageName())));
						startActivity(intent);
					} catch (Exception e) {
						Intent intent = new Intent();
						intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
						startActivity(intent);
					}
				} else {
					//below android 11
					ActivityCompat.requestPermissions(MainActivity.this, new String[]{WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
				}
			});
			builder.setNegativeButton(R.string.app_cancel, (dialog, id) -> dialog.cancel());
			AlertDialog dialog = builder.create();
			dialog.show();
			Objects.requireNonNull(dialog.getWindow()).setGravity(Gravity.BOTTOM);
		}
	}
}
