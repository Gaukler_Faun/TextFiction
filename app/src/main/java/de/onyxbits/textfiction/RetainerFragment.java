package de.onyxbits.textfiction;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.onyxbits.textfiction.zengine.GrueException;
import de.onyxbits.textfiction.zengine.ZMachine;
import de.onyxbits.textfiction.zengine.ZMachine3;
import de.onyxbits.textfiction.zengine.ZMachine5;
import de.onyxbits.textfiction.zengine.ZMachine8;
import de.onyxbits.textfiction.zengine.ZScreen;
import de.onyxbits.textfiction.zengine.ZStatus;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.fragment.app.Fragment;

import static de.onyxbits.textfiction.LoaderTask.createMemImage;
import static de.onyxbits.textfiction.LoaderTask.extractGame;
import static de.onyxbits.textfiction.LoaderTask.isBlorb;

/**
 * Initializing a game is expensive and loosing gamestate on rotation would
 * bother the user. This fragment is simply a container for holding stuff that
 * needs to survive between configuration changes. Details:
 * 
 * http://www.androiddesignpatterns.com/2013/04/retaining-objects-across-config-
 * changes.html
 * 
 * @author patrick
 * 
 */
public class RetainerFragment extends Fragment {

	/**
	 * The engine of the currently running game
	 */
	public ZMachine engine;

	/**
	 * Stores the narrator/user messages.
	 */
	public List<StoryItem> messageBuffer;

	/**
	 * Always surround engine.run() in try/catch and put the caught exception
	 * here, call finish() afterwards.
	 */
	public GrueException postMortem;

	/**
	 * Contents of the upper window
	 */
	public String upperWindow;
	
	/**
	 * Words that should be highlighted in the story
	 */
	public Vector<String> highlighted;

	public RetainerFragment() {
		messageBuffer = new Vector<>();
		highlighted = new Vector<>();
		upperWindow = "";
		postMortem = null;
		engine = null;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Retain this fragment across configuration changes.
		// setRetainInstance(true); -> Manifest

		if (engine == null) {
			Activity activity = getActivity();
			assert activity != null;
			File f = new File(activity.getIntent().getStringExtra(GameActivity.LOADFILE));

			ExecutorService executor = Executors.newSingleThreadExecutor();
			Handler handler = new Handler(Looper.getMainLooper());

			executor.execute(() -> {
				//Background work here
				ZMachine engine = null;
				try {
					byte[] memImage = createMemImage(new FileInputStream(f));

					if (isBlorb(memImage)) {
						memImage = extractGame(memImage);
					}

					switch (memImage[0]) {
						case 3: {
							engine = new ZMachine3(new ZScreen(), new ZStatus(), memImage);
							break;
						}
						case 5: {
							engine = new ZMachine5(new ZScreen(), memImage);
							break;
						}
						case 8: {
							engine = new ZMachine8(new ZScreen(), memImage);
							break;
						}
					}
				}
				catch (IOException e) {
					Log.w(getClass().getName(), e);
				}
				catch (ArrayIndexOutOfBoundsException e) {
					Log.w(getClass().getName(), "Empty file");
				}

				if (engine != null) {
					try {
						// NOTE: Don't throw a command save in the same directory as a menu
						// save. It won't work! The menusave captures the state of the machine
						// at rest. The command save captures the state in the middle of
						// execution (while executing the save commmand).
						engine.restart();
						engine.run();
					}
					catch (GrueException e) {
						Log.w(getClass().getName(), e);
						engine = null;
					}
				}

				ZMachine finalEngine = engine;
				handler.post(() -> {
					//UI Thread work here
					this.engine = finalEngine;

					GameActivity gameActivity = (GameActivity) this.getActivity();
					if (gameActivity != null) {
						if (finalEngine != null) {
							gameActivity.publishResult();
						} else {
							this.postMortem = new GrueException(gameActivity.getString(R.string.msg_corrupt_game_file));
							gameActivity.finish();
						}
					}
				});
			});
		}
	}
}
